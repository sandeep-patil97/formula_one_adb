# Databricks notebook source
# MAGIC %run ../utils/common_functions

# COMMAND ----------

input_schema = StructType(
    [
        StructField("code", StringType()),
        StructField("dob", StringType()),
        StructField("driverId", IntegerType()),
        StructField("driverRef", StringType()),
        StructField("name", StructType([
            StructField("forename",StringType()),
            StructField("surname",StringType())]   
        )),
        StructField("nationality", StringType()),
        StructField("number", StringType()),
        StructField("url", StringType())  
    ]
)

# COMMAND ----------

df = spark.read.json('/mnt/bronze/drivers.json',schema=input_schema)

# COMMAND ----------

df = df.withColumnRenamed('driverId','driver_id').withColumnRenamed('driverRef','driver_ref').withColumn('ingest_dt',lit(current_dt))

# COMMAND ----------

df.select(df['name']['forename']).display()

# COMMAND ----------

df = df.withColumn('forename',df['name']['forename']).withColumn('surname',df['name']['surname'])

# COMMAND ----------

df = df.drop("name")

# COMMAND ----------

df.display()

# COMMAND ----------

df.write.mode('overwrite').parquet('/mnt/silver/drivers')
