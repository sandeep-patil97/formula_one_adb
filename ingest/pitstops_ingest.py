# Databricks notebook source
# MAGIC %run ../utils/common_functions

# COMMAND ----------

input_schema = StructType(
    [
        StructField("raceId", IntegerType()),
        StructField("driverId", IntegerType()),
        StructField("stop", IntegerType()),
        StructField("lap", IntegerType()),
        StructField("time", StringType()),
        StructField("duration", StringType()),
        StructField("milliseconds", IntegerType())
    ]
)

# COMMAND ----------

spark.read.json('/mnt/bronze/pit_stops.json', schema=input_schema, multiLine=True).display()

# COMMAND ----------

df = spark.read.json('/mnt/bronze/pit_stops.json', schema=input_schema, multiLine=True)

# COMMAND ----------

df = df.withColumnRenamed('raceId','race_id').withColumnRenamed('driverId','driver_id').withColumn('ingest_dt',lit(current_dt))

# COMMAND ----------

df.display()

# COMMAND ----------

df.write.mode('overwrite').parquet('/mnt/silver/pitstops')
