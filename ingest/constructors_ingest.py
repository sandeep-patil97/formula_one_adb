# Databricks notebook source
# MAGIC %run ../utils/common_functions

# COMMAND ----------

# Defining Schema

input_schema = StructType(
    [
        StructField("constructorId", IntegerType()),
        StructField("constructorRef", StringType()),
        StructField("name", StringType()),
        StructField("nationality", StringType()),
        StructField("url", StringType())
    ]
)

# COMMAND ----------

display(dbutils.fs.ls('/mnt/bronze'))

# COMMAND ----------

spark.read.json('/mnt/bronze/constructors.json',schema=input_schema).printSchema()

# COMMAND ----------

df = spark.read.json('/mnt/bronze/constructors.json',schema=input_schema)
df.display()

# COMMAND ----------

# rename column and add new column with current date

df = df.withColumnRenamed('constructorId','constructor_id').withColumnRenamed('constructorRef','constructor_ref').withColumn('ingest_dt',lit(current_dt))

# COMMAND ----------

df.display()

# COMMAND ----------

df.write.mode('overwrite').parquet('/mnt/silver/constructors')

# COMMAND ----------

spark.read.parquet('/mnt/silver/constructors').display()
