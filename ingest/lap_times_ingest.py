# Databricks notebook source
# MAGIC %run ../utils/common_functions

# COMMAND ----------

input_schema = StructType(
    [
        StructField("raceId", IntegerType()),
        StructField("driverId", IntegerType()),
        StructField("lap", IntegerType()),
        StructField("position", IntegerType()),
        StructField("time", StringType()),
        StructField("milliseconds", IntegerType())
    ]
)

# COMMAND ----------

df = create_csv_df('/mnt/bronze/lap_times',input_schema,False)

# COMMAND ----------

df = (
    df.withColumnRenamed("raceId", "race_id")
    .withColumnRenamed("driverId", "driver_id")
    .withColumn("ingest_dt", lit(current_dt))
)

# COMMAND ----------

df.display()

# COMMAND ----------

df.write.mode('overwrite').parquet('/mnt/silver/lap_times')
