# Databricks notebook source
# MAGIC %run ../utils/common_functions

# COMMAND ----------

spark.read.json('/mnt/bronze/results.json').printSchema()

# COMMAND ----------

input_schema = StructType(
    [
        StructField("constructorId", IntegerType()),
        StructField("driverId", IntegerType()),
        StructField("fastestLap", StringType()),
        StructField("fastestLapSpeed", StringType()),
        StructField("fastestLapTime", StringType()),
        StructField("grid", IntegerType()),
        StructField("laps", IntegerType()),
        StructField("milliseconds", StringType()),
        StructField("number", StringType()),
        StructField("points", FloatType()),
        StructField("position", StringType()),
        StructField("positionOrder", IntegerType()),
        StructField("positionText", StringType()),
        StructField("raceId", IntegerType()),
        StructField("rank", StringType()),
        StructField("resultId", IntegerType()),
        StructField("statusId", IntegerType()),
        StructField("time", StringType())  
    ]
)

# COMMAND ----------

df = spark.read.json('/mnt/bronze/results.json',schema=input_schema)

# COMMAND ----------

df.display()

# COMMAND ----------

df = (
    df.withColumnRenamed("constructorId", "constructor_id")
    .withColumnRenamed("driverId", "driver_id")
    .withColumnRenamed("resultId", "result_id")
    .withColumnRenamed("raceId", "race_id")
    .withColumnRenamed("statusId", "status_id")
    .withColumnRenamed("grid", "gr_id")
    .withColumn("ingest_dt", lit(current_dt))
)

# COMMAND ----------

df.display()

# COMMAND ----------

df.write.mode('overwrite').parquet('/mnt/silver/results')
