# Databricks notebook source
# MAGIC %md
# MAGIC 1. Read CSV File
# MAGIC 2. apply schema for it
# MAGIC 3. rename and remove column based on the requirement

# COMMAND ----------

# MAGIC %run ../utils/common_functions

# COMMAND ----------

dbutils.widgets.text('source_nm',"circuits")
source_nm = dbutils.widgets.get('source_nm')

# COMMAND ----------


# Defining schema

input_schema = StructType(
    [
        StructField("circuitId", IntegerType()),
        StructField("circuitRef", StringType()),
        StructField("name", StringType()),
        StructField("location", StringType()),
        StructField("country", StringType()),
        StructField("lat", FloatType()),
        StructField("lng", FloatType()),
        StructField("alt", IntegerType()),
        StructField("url", StringType())
    ]
)

# COMMAND ----------

df = create_csv_df(f"/mnt/bronze/{source_nm}.csv",input_schema)

# COMMAND ----------

# rename column

df = df.withColumnRenamed('circuitId','circuit_id')
df = df.withColumnRenamed('circuitRef','circuit_ref')

# COMMAND ----------

df = df.withColumn('ingest_dt',lit(current_dt))

# COMMAND ----------

df.display()

# COMMAND ----------

df.write.mode('overwrite').parquet(f"/mnt/silver/{source_nm}")

# COMMAND ----------

display(dbutils.fs.ls('/mnt/silver/circuits'))
