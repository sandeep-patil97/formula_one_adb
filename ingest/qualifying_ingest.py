# Databricks notebook source
# MAGIC %run ../utils/common_functions

# COMMAND ----------

display(dbutils.fs.ls('/mnt/bronze/qualifying'))

# COMMAND ----------

spark.read.json('/mnt/bronze/qualifying',multiLine=True).printSchema()

# COMMAND ----------

# MAGIC %md
# MAGIC In this df, we not set schema separately. we directly went with default schema as these are json files.

# COMMAND ----------

df = spark.read.json('/mnt/bronze/qualifying',multiLine=True)

# COMMAND ----------

df = (
    df.withColumnRenamed("constructorId", "constructor_id")
    .withColumnRenamed("driverId", "driver_id")
    .withColumnRenamed("qualifyId", "qualify_id")
    .withColumnRenamed("raceId", "race_id")
    .withColumn("ingest_dt", lit(current_dt))
)

# COMMAND ----------

df.display()

# COMMAND ----------

df.write.mode('overwrite').parquet('/mnt/silver/qualifying')

# COMMAND ----------

spark.read.parquet('/mnt/silver/qualifying').display()
