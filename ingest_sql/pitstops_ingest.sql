-- Databricks notebook source
create table if not exists formulaone_db.pit_stops(
  raceId integer,
  driverId integer,
  stop integer,
  lap integer,
  time string,
  duration string,
  milliseconds integer
) using json location '/mnt/bronze/pit_stops.json' options (multiLine 'true')

-- COMMAND ----------

select * from formulaone_db.pit_stops limit 1;
