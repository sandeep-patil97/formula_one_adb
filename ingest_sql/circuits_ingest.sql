-- Databricks notebook source
create database if not exists formulaone_db

-- COMMAND ----------

desc database formulaone_db

-- COMMAND ----------

drop table formulaone_db.circuits;

-- COMMAND ----------

create table if not exists formulaone_db.circuits
(
  circuitId integer,
  circuitRef string,
  name string,
  location string,
  country string,
  lat float,
  lng float,
  alt integer,
  url string
) using csv location '/mnt/bronze/circuits.csv' options (header 'true')
 

-- COMMAND ----------

select * from formulaone_db.circuits;
