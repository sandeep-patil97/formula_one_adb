# Databricks notebook source
# MAGIC %md
# MAGIC #### Mounting bronze layer to ADB

# COMMAND ----------

dbutils.widgets.text("layer_nm","bronze")
layer_nm = dbutils.widgets.get("layer_nm")

# COMMAND ----------

storage_account_name = "bwtformulaone79"

# COMMAND ----------

def mount_adls(storage_account_name, container_name):

    accesskey = "Fcc3TjjeI+NNxhQfWsZ/0hD0LRk3hKeyuo+ADoZsnJpdI9TuEy0pftTaeRBDMwckslK3b/Wni8W0+AStLWeNfw=="

    # Set spark Configs
   
    configs = {f"fs.azure.account.key.{storage_account_name}.blob.core.windows.net": accesskey}
    
    # Mount Storage Account Containers
    dbutils.fs.mount(
             source = f"wasbs://{layer_nm}@{storage_account_name}.blob.core.windows.net/",
             mount_point = f"/mnt/{layer_nm}",
             extra_configs = configs)

# COMMAND ----------

mount_adls("bwtformulaone79",layer_nm)

# COMMAND ----------

display(dbutils.fs.mounts())
