# Databricks notebook source
display(dbutils.fs.ls('/mnt/silver/results'))

# COMMAND ----------

# MAGIC %sql
# MAGIC create or replace view formulaone_db.results_view as
# MAGIC select * from parquet.`/mnt/silver/results`;

# COMMAND ----------

# MAGIC %sql
# MAGIC desc formulaone_db.results_view

# COMMAND ----------

# MAGIC %sql
# MAGIC select race_id,driver_id,points,position,positionText from formulaone_db.results_view ;

# COMMAND ----------

race_df = spark.read.parquet('/mnt/silver/race')

# COMMAND ----------

race_df.columns

# COMMAND ----------

from pyspark.sql.functions import col,year,sum
race_df = race_df.withColumn('year_dt',year(col('date'))).select('race_id','year_dt')
race_df.display()

# COMMAND ----------

result_df = spark.sql("select * from parquet.`/mnt/silver/results`").select('driver_id','race_id','constructor_id','points','position','positionText')
result_df.columns

# COMMAND ----------

race_result_df = result_df.join(race_df,'race_id','left')
race_result_df.display()

# COMMAND ----------

from pyspark.sql.functions import when, count, rank, lit, desc
from pyspark.sql.window import Window

driverstands_df = race_result_df.groupBy("year_dt", "driver_id").agg(
    sum("points").alias("total_points"),
    count(when(col("position") == 1, 1)).alias("win"),
)

driverstands_df=driverstands_df.withColumn(
    "driverstanding_id",
    rank().over(
        Window.partitionBy("year_dt").orderBy(desc("total_points"), desc("win"))
    ),
)

# COMMAND ----------

from pyspark.sql.functions import concat
driver_df = spark.read.parquet('/mnt/silver/drivers').select('driver_id',concat('forename', lit(" "),'surname').alias('driver_nm'))

# COMMAND ----------

driver_detail_df = driverstands_df.join(driver_df,'driver_id','inner')

# COMMAND ----------

driver_cons = result_df.select("driver_id",'constructor_id').rdd.map(lambda x : x.asDict()).collect()

# COMMAND ----------

print(driver_cons)

# COMMAND ----------

driver_cons_res = {}
for i in driver_cons:
    driver_cons_res[i['driver_id']] = i['constructor_id']

print(driver_cons_res)

# COMMAND ----------

def get_const_id(driver_id):
    return driver_cons_res.get(driver_id)

from pyspark.sql.functions import udf
from pyspark.sql.types import IntegerType
get_const_id_udf = udf(get_const_id,IntegerType())

# COMMAND ----------

driver_team_details_df = driver_detail_df.withColumn("team_id",get_const_id_udf(col('driver_id')))

# COMMAND ----------

constructor_df = spark.read.parquet('/mnt/silver/constructors').select('constructor_id','name')

# COMMAND ----------

driver_team_details_df1 = driver_team_details_df.join(constructor_df,driver_team_details_df.team_id == constructor_df.constructor_id,'left')

# COMMAND ----------

formula_one_standing_df = driver_team_details_df1.select('driverstanding_id','driver_nm',col('name').alias('team_name'),'win','total_points','year_dt')

# COMMAND ----------

formula_one_standing_df.display()

# COMMAND ----------

formula_one_standing_df.write.mode('overwrite').parquet('/mnt/gold/formula_one_standing')

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from parquet.`/mnt/gold/formula_one_standing`
