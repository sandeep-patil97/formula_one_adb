# Databricks notebook source
# MAGIC %run ../utils/common_functions

# COMMAND ----------

display(dbutils.fs.ls('/mnt/silver'))

# COMMAND ----------

driver_df = spark.read.parquet('/mnt/silver/drivers/')
constructor_df = spark.read.parquet('/mnt/silver/constructors/')
qualifying_df = spark.read.parquet('dbfs:/mnt/silver/qualifying/')

# COMMAND ----------

qualifying_df1 = (
    qualifying_df.select(
        "driver_id",
        "constructor_id",
        col("q1").alias("qualifying1"),
        col("q2").alias("qualifying2"),
        col("q3").alias("qualifying3"),
    )
    .withColumn("qualifying1", regexp_replace("qualifying1", "\\\\N", ""))
    .withColumn("qualifying2", regexp_replace("qualifying2", "\\\\N", ""))
    .withColumn("qualifying3", regexp_replace("qualifying3", "\\\\N", ""))
)

# COMMAND ----------

driver_df1 = driver_df.withColumn('driver',concat('forename',lit(" "),'surname')).select('driver_id','number','driver')

# COMMAND ----------

constructor_df1 = constructor_df.select('constructor_id',col('name').alias('team'))

# COMMAND ----------

result_df = qualifying_df1.join(driver_df1,'driver_id','left').join(constructor_df1,'constructor_id','left')

# COMMAND ----------

result_df.columns

# COMMAND ----------

result_col_lst = ['driver','number','team','qualifying1','qualifying2','qualifying3']
result_df1 = result_df.select(*result_col_lst)

# COMMAND ----------

result_df1.write.mode('overwrite').parquet('/mnt/gold/qualifying_result')
